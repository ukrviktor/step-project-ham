const imagesMenuItemLink = 'images-menu__item-link';
const countItemsContent = [12, 24, 36];
const maxCountPushLoadBtn = 2;
let currentCountPushLoadBtn = 0;

const imagesMenu = document.querySelector('.our-work__images-menu');
const imagesContent = document.querySelector('.our-work__images-content');
const imagesLoadMoreBtn = document.querySelector('.our-work__button');

const all = {
  textForFirstItem: 'All Projects',
  datasetValue: 'all-projects',
};
const graphicDesign = {
  textForFirstItem: 'Graphic Design',
  datasetValue: 'graphic-design',
  path: './img/our_amazing_work/graphic-design/',
  items: [
    'graphic-design1.jpg',
    'graphic-design2.jpg',
    'graphic-design3.jpg',
    'graphic-design4.jpg',
    'graphic-design5.jpg',
    'graphic-design6.jpg',
    'graphic-design7.jpg',
    'graphic-design8.jpg',
    'graphic-design9.jpg',
    'graphic-design10.jpg',
    'graphic-design11.jpg',
    'graphic-design12.jpg',
  ],
};
const webDesign = {
  textForFirstItem: 'Web Design',
  datasetValue: 'web-design',
  path: './img/our_amazing_work/web-design/',
  items: [
    'web-design1.jpg',
    'web-design2.jpg',
    'web-design3.jpg',
    'web-design4.jpg',
    'web-design5.jpg',
    'web-design6.jpg',
    'web-design7.jpg',
    'web-design8.jpg',
    'web-design9.jpg',
    'web-design10.jpg',
    'web-design11.jpg',
    'web-design12.jpg',
  ],
};
const landingPages = {
  textForFirstItem: 'Landing Pages',
  datasetValue: 'landing-pages',
  path: './img/our_amazing_work/landing-page/',
  items: [
    'landing-page1.jpg',
    'landing-page2.jpg',
    'landing-page3.jpg',
    'landing-page4.jpg',
    'landing-page5.jpg',
    'landing-page6.jpg',
    'landing-page7.jpg',
    'landing-page8.jpg',
    'landing-page9.jpg',
    'landing-page10.jpg',
    'landing-page11.jpg',
    'landing-page12.jpg',
  ],
};
const wordpress = {
  textForFirstItem: 'Wordpress',
  datasetValue: 'wordpress',
  path: './img/our_amazing_work/wordpress/',
  items: [
    'wordpress1.jpg',
    'wordpress2.jpg',
    'wordpress3.jpg',
    'wordpress4.jpg',
    'wordpress5.jpg',
    'wordpress6.jpg',
    'wordpress7.jpg',
    'wordpress8.jpg',
    'wordpress9.jpg',
    'wordpress10.jpg',
    'wordpress11.jpg',
    'wordpress12.jpg',
  ],
};

function handlerImagesMenu(e) {
  const item = e.target;

  if (
    item.classList.contains(imagesMenuItemLink) &&
    !item.classList.contains(nameCssActive)
  ) {
    currentCountPushLoadBtn = 0;
    imagesLoadMoreBtn.classList.remove('disable');
    clearStateActive(imagesMenu.children);
    setStateActive(item);
    changeImagesContent();
  }
}

function handlerLoadMoreBtn() {
  loaderWrapper.classList.add('enable');
  setTimeout(() => {
    loaderWrapper.style.opacity = 1;
  }, 300);

  setTimeout(() => {
    setTimeout(() => {
      loaderWrapper.classList.remove('enable');
    }, 1000);
    loaderWrapper.style.opacity = 0;

    currentCountPushLoadBtn++;

    changeImagesContent();

    if (currentCountPushLoadBtn === maxCountPushLoadBtn) {
      imagesLoadMoreBtn.classList.add('disable');
    }
  }, 2400);
}

function changeImagesContent() {
  const imagesMenuItemActive = getStateActive(imagesMenu.children);
  const nameObjItems = getObjNameFromDataset(imagesMenuItemActive);

  if (nameObjItems) {
    let i = 0;
    let countImage = 0;

    clearImagesContent();

    for (
      let count = 0; count < countItemsContent[currentCountPushLoadBtn]; count++
    ) {
      i = Math.floor(Math.random() * nameObjItems.length);

      imagesContent.insertAdjacentHTML(
        'beforeend',
        imageItemContent(
          nameObjItems[i].textForFirstItem,
          nameObjItems[i].datasetValue,
          nameObjItems[i].path,
          nameObjItems[i].items[countImage]
        )
      );
      countImage =
        countImage >= nameObjItems[i].items.length - 1 ? 0 : countImage + 1;
    }
  } else {
    alert('Error: not find nameObjItems');
  }
}

function clearImagesContent() {
  imagesContent.innerHTML = '';
}

function imageItemContent(text, datasetValue, path, name) {
  return `
  <div class="images-content__item" data-images-item-link="${datasetValue}">
    <div class="images-content__item-front">
      <div class="images-content__item-img">
        <img src="${path}${name}" alt="${text}" />
      </div>
    </div>

    <div class="images-content__item-back">
      <div class="images-content__item-text">
        <ul class="item-text__circles">
          <li class="button-circle-white">
            <i class="fa-sharp fa-solid fa-link"></i>
          </li>
          <li class="button-circle-green">
            <i class="fa-sharp fa-solid fa-square"></i>
          </li>
        </ul>
        <div class="item-text__title text-green">
          creative design
        </div>
        <div class="item-text__subtitle">${text}</div>
      </div>
    </div>
  </div>
  `;
}

function getObjNameFromDataset(item) {
  let result;

  switch (item.dataset.imagesMenuLink) {
    case 'all-projects':
      result = [graphicDesign, webDesign, landingPages, wordpress];
      break;
    case 'graphic-design':
      result = [graphicDesign];
      break;
    case 'web-design':
      result = [webDesign];
      break;
    case 'landing-pages':
      result = [landingPages];
      break;
    case 'wordpress':
      result = [wordpress];
      break;
    default:
      result = false;
  }
  return result;
}

imagesMenu.addEventListener('click', handlerImagesMenu);
imagesLoadMoreBtn.addEventListener('click', handlerLoadMoreBtn);

changeImagesContent();