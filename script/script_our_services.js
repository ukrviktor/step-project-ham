const tabsMenuItemLink = 'tabs-menu__item-link';
const tabsMenu = document.querySelector('.our-services__tabs-menu');
const tabsContent = document.querySelector('.our-services__tabs-content');

tabsMenu.addEventListener('click', changeTabsMenuItemActive);

function changeTabsMenuItemActive(e) {
  const item = e.target;
  if (
    item.classList.contains(tabsMenuItemLink) &&
    !item.classList.contains(nameCssActive)
  ) {
    clearStateActive(tabsMenu.children);
    setStateActive(item);
    changeTabsContentItemActive(item);
  }
}

function changeTabsContentItemActive(item) {
  const contentItem = getTabsConnectedContent(item);

  if (contentItem) {
    clearStateActive(tabsContent.children);
    setStateActive(contentItem);
  } else {
    alert('Error: not finded connectedContentItem');
  }
}

function getTabsConnectedContent(item) {
  for (let i of tabsContent.children) {
    if (i.dataset.item === item.dataset.item) {
      return i;
    }
  }
  return false;
}