const galleryContainer = document.querySelector('.gallery__images-content');
const galleryImagesLoadMoreBtn = document.querySelector('.gallery__button');

const countGalleryItemsContent = [2, 4];
const maxGalleryCountPushLoadBtn = 2;
let currentGalleryCountPushLoadBtn = 0;

let gallery;
const galleryLoader = imagesLoaded(galleryContainer);

function handlerGalleryLoader(e) {
  imagesLoaded(galleryContainer, function () {
    gallery = new Masonry(galleryContainer, {
      itemSelector: '.gallery__image-item',
      columnWidth: '.gallery__image-item-sizer',
      percentPosition: true,
      gutter: 15,
      isAnimated: true,
      animationOptions: {
        queue: false,
        duration: 500,
      },
    });
  });
}

function handlerGalleryLoadMoreBtn() {
  loaderWrapper.classList.add('enable');
  setTimeout(() => {
    loaderWrapper.style.opacity = 1;
  }, 300);

  setTimeout(() => {
    setTimeout(() => {
      loaderWrapper.classList.remove('enable');
    }, 1000);
    loaderWrapper.style.opacity = 0;

    addedNewImages();

    currentGalleryCountPushLoadBtn++;

    if (currentGalleryCountPushLoadBtn === maxGalleryCountPushLoadBtn) {
      galleryImagesLoadMoreBtn.classList.add('disable');
    }
  }, 2400);
}

function addedNewImages() {
  const addImages = document.querySelectorAll('.gallery__image-item-disable');
  for (
    let count = 0; count < countGalleryItemsContent[currentGalleryCountPushLoadBtn]; count++
  ) {
    addImages[count].classList.remove('gallery__image-item-disable');
    addImages[count].classList.add('gallery__image-item');
    gallery.addItems(addImages[count]);
    gallery.layout();
  }
}

galleryLoader.on('always', handlerGalleryLoader);
galleryImagesLoadMoreBtn.addEventListener('click', handlerGalleryLoadMoreBtn);